const controller = {};

controller.list = (req, res) => {
  req.getConnection((err, conn) => {
    conn.query('SELECT * FROM student', (err, students) => {
      if (err) {
        res.json(err);
      }
      res.render('students', {
        data: students
      });
    });
  });
};

controller.save = (req, res) => {
  const data = req.body;
  console.log(req.body)
  req.getConnection((err, connection) => {
    const query = connection.query('INSERT INTO student set ?', data, (err, student) => {
      console.log(student)
      res.redirect('/');
    })
  })
};

controller.edit = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query("SELECT * FROM student WHERE id = ?", [id], (err, rows) => {
      res.render('student_edit', {
        data: rows[0]
      })
    });
  });
};

controller.update = (req, res) => {
  const { id } = req.params;
  const newStudent = req.body;
  req.getConnection((err, conn) => {

  conn.query('UPDATE student set ? where id = ?', [newStudent, id], (err, rows) => {
    res.redirect('/');
  });
  });
};

controller.delete = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, connection) => {
    connection.query('DELETE FROM student WHERE id = ?', [id], (err, rows) => {
      res.redirect('/');
    });
  });
}



module.exports = controller;
