CREATE database crudstudents;

use crudstudents;

CREATE TABLE student (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  surname VARCHAR(100) NOT NULL,
  middle_grade float(15)
);


SHOW TABLES;

describe student;
